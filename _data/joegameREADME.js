

module.exports = async function() {
  const res = await require("node-fetch")("https://gitlab.com/api/v4/projects/27054517/repository/files/README.md?ref=main")
  const json = await res.json()
  const buff = Buffer.from(json.content, 'base64')
  return buff.toString('utf-8')
}
