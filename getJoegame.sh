#! /bin/bash
# This script will download the latest joegame artifacts from the "main" branch and set it up in first level
# subdirector
# It cleans itself up
JGAME=joegame_artifact.zip

if [ -f "$JGAME" ]; then
  echo "already have artifacts!"
else
  curl --location --output joegame_artifact.zip "https://gitlab.com/api/v4/projects/27054517/jobs/artifacts/main/download?job=pages"
fi
unzip joegame_artifact.zip -d j_temp
mv -f j_temp/public _site/joegame
rm -rf j_temp
