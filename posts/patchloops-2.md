---
date: "2023"
---
# Patchloops (pt2)

### p8/14G/2: analysis

A pure type 2 patch composed of light blues, dark blues, and yellows.  The rather straightforward pallet is fairly uncharacteristic of a non-hybrid type 2, placing this patch firmly in the &ldquo;middle&rdquo; period of type 2 patches.  The most significant feature of the patch is the rare &ldquo;nib&rdquo; on its primary face: a small knot on loop X10 protrudes from the main face, creating the famous &ldquo;14G nib.&rdquo; 

While the nib on 14G broadly singularizes it among the canon, it is variously grouped among several patch genres.  Most popularly, it is grouped with patches such as 2A7 (&ldquo;Mountain&rdquo;) and 3EL, namely the &ldquo;type 2 representatives&rdquo; (Schieling 2014).  Some newer schools regard this among the &ldquo;early ombré patches,&rdquo; respectively grouping it with the 1G0 and 214.    

### p8/14G/2: interpretation

This patch deals with concepts of subjectivity and place.  Personality, as we know, is the mere sedimentation of stimuli along a semi-permeable, semi-resistant membrane.  As rocks split and smooth in the river, so does the styled ego of the individual from the stream of impressions the world provides.

Among the many implications of this concept of the individual is a newfound appreciation for precious moment of introspection.  A life dominated by repitition and humdrum is void of introspective access for the simple reason that it is only when things change can one really separate themselves for their surroundings. Only when life alters can we individuate the individual, and ask questions of them. 

This is the general lesson of what commentators call the &ldquo;nib&rdquo; of 14G.  The new dimensionality the nib finds among the universe of aesthetic patchloop-features, performs a kind psychological story: its relief places the subject, and interrogates them.  It reminds us that we are in fact not strangers to ourselves, and that confidence comes from clearing away internal opacity, not disregarding others.  


<a id="org75a5f79"></a>

## DONE type 1 w/ purple


<a id="org98d6f6b"></a>

### person

larndz


<a id="org1bc3dc1"></a>

### id

8-3-9
p8/2AB/1


<a id="org6657fa4"></a>

### analysis

One of three known &ldquo;pure&rdquo; type 1 patches.  This type classification alone distinguishes 2AB, and pairs it with the other commonly circulated pure type 1 loop patch, p8/6H.  
The horizontal set presents with a banal pattern of stark, neon colors, while the vertical set retains 16 purple loops with two white loops on X1 and X18.       
Using Corey&rsquo;s method (1993) for parsing the pattern in the horizontal set, the resulting matrix is
X00XXX00XX00XXX00X
0X00000X00X00000X0
00X000X0000X000X00


<a id="orgeaeec27"></a>

### interpretation

Baker Corey is one of the more recognizable names in modern patch analysis. Her work in various methods of patch serialization created a wave of new literature in early patchloops, for better and worse.   

A less well known theory Corey cultivated, one that she once noted as the &ldquo;the proper goal for all patch serialization efforts&rdquo; (*Love Loops*, 1999), is the concept of &ldquo;rhythm&rdquo; in patches.  Corey thought of this effort as a kind of makeshift Fourier transformation of patch loops, decomposing the immanent, totalizing *signal* of patches into constitutive &ldquo;rhythms&rdquo;, expressing a radical temporality to early vertical-static patches.

Corey serialization in 2AB draws a kind of cascading series of accents, radiating around a tunnel.  We read the spirit of Corey&rsquo;s work in this patch: as differentiation is registered in the time domain, so one can find it in various constitutive frequencies.  As above as below.  Consider how your life and body are two manifestations of one ultimately anonymous experience, and that there is always two ways to say the same thing.


<a id="org46f967e"></a>

## DONE type 1 w/ white


<a id="orgecf56a3"></a>

### id

p8/6H/1


<a id="orge5f5bc8"></a>

### analysis

Along with, 2AB, 6H is the other main &ldquo;pure&rdquo; type 1 patch of the second canon.  Compared to 2AB, 6H&rsquo;s pallet is relatively restrained, with only three distinctive colors. Like of most patches of this era, the horizontal set is functional background (here, white); the vertical set loops give a simple, alternating yellow and blue pattern.

While its restrained pallet is generally a feature of type 1 dominated or pure patches, 6H is sometimes grouped with &ldquo;the symbolic array&rdquo; of patches first named by Goipin in 1980, and generally defined by their  palletes of three. 


<a id="org2c623ac"></a>

### interpretation

6H is a patch of youth, home, and meditation.  The yellow and blue foregrounding colors are reminiscent of p8/2A7 (the famous &ldquo;Mountain&rdquo; patch), stripped of their organic background, and applied to the stark, minimized background of a domestic room.  

That is, moving from 2A7 to 6H is moving from a deterritorialized, arboreal landscape, to a bedroom, only sparsely decorated.  The dappled drywall across from you paints streaming blue and yellow lines, borrowed in constrained ways from the outside world.  

All the sounds of the day ring in a dull fade as your little toy sky presents itself on the wall, and your breath becomes more implicated in your thoughts.  You consider the way your fingers seem to recreate your thoughts as they move around in your hand.  You chuckle.  You are at home.   


<a id="orgac1b708"></a>

## DONE candy


<a id="orgd448079"></a>

### id

7-7-1
p8/26F/2


<a id="orge0fab31"></a>

### analysis

26F is known as the &ldquo;candy&rdquo; patch and it is a mature non-oriented patch of the second canon.  Non-orientation patches are defined by the agnosticism of function between the vertical and horizontal set of loops in creating a visual representation, and typically are considered &ldquo;emotive&rdquo; in this.

The &ldquo;candy patch&rdquo; nickname comes from Pinkston and Corey&rsquo;s 1986 paper on emotive patches, and is &ldquo;understandably&rdquo; one of the more popular patches in the kitchen revivalist movement of patchloops (Crikmon 2017, Calley 2018). 

Both sets are dominated by a wide spectrum of pinks, blues, and purples.


<a id="org4b2862a"></a>

### interpretation

Commentator&rsquo;s can get quite passionate talking about some of these second-canon emotive patches; largely, in this committee&rsquo;s opinion, to a fault.  Emotive patches should be moments of intellectual reprieve, and associative play, not heady ventures into crypto-Freudianism.   

In this, we take *candy* as it is:  a pile of goop, held together by will and potential, ever renewing itself as a simple affirmation of affection itself.

When Cartlin gave his famous speech at 1988&rsquo;s Loop Conference in San Francisco, he ended with a short anecdote on 26F:

&ldquo;I remember waking up at my cousins house in Arvada in 1956, putting my boots on to go see the sky.  Walking past my open journal in the desk of the guest room, I remember seeing the sketch of *candy*, and just stopping in my tracks: how long had it took me to actually see it?  Because it was right in that moment    


<a id="orgc645c37"></a>

## DONE quad orange blue


<a id="org840921e"></a>

### id

p8/196/&21


<a id="org45806bb"></a>

### analysis

p8/196 is a classic &ldquo;quad&rdquo; type 2 hybrid patch, exclusively composed of blues and oranges.  Like virtually all quad-types, a properly oriented patch will have two solid 8x8 squares, one on the bottom right and another top left.  For 196, this means a solid orange box bottom right, and blue upper left.    

Because both the horizontal and vertical sets take part in the conceptual framework of quad patches, and because they have limited palletes by necessity, many commentators call them &ldquo;emotive by default&rdquo; and group them accordingly.  

Some patches in horizontal set are more red than orange, but few have considered this to trouble the overall consistency or “quadness” of the patch.


<a id="orgd1ab06b"></a>

### interpretation

Quad patches emphasize concepts of psychological taxonomy and health.  196 is a fiery exercise of concept and projection.  
A common anxiety in the early 21st century pertains to issues of legitimacy in beliefs and thoughts.  Namely, it is *not*



