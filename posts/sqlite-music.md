---
title: Supercollider and Sqlite adventures
tags: tech 
date: 2021-03-12
---

Much of the internet is `databases.` In particular, a lot of the "business side"
of our interactions and habits with computers is that threshold where the user
has (heavily mediated) interactions with a database. The necessity and
application of databases brings out what is "anonymous" about everything that is
not a database in computers. That is, a computer program has generally limited
cababilities in tracking ongoing changes that must persist between the time the
program ends, and it starts up again. When you write a computer program, you
don't make something that will be one thing and culminate to something else. A
computer program only grows when the programmer adds to it, everything else is
just the way it behaves when you feed it. And so the special kind of program of
the database comes to bring the functionality a compiled binary cannot:
exteriority, persistence, and the assurance of a 'record.'

In some ways this distinction reprises ancient problems, for where does the
experience of the computer come from: the database, or the view of the database?
What is art, the form represented, or its in-fact existence? What is a subject,
a transcendental form giver? Or is the giving from the other way?

The continual progress of consumer computing, social media, and the various
amenities, now near-universal, related to the global networks of those in power
produces an individual that would want to say at this point: "the dry, practical
database provides us with the functionality we need in order to provide the
service to the user; it is no more or less important than the color of the
buttons, or the space between then tabs. Yes, it is a weird kind of progam, I
don't `code` my database, I query it, but they are just one of many parts of the
stack necessary to provide the experience."

While this is probably a very real experience for your typical "full-stack"
developer, it falls into a common misunderstanding of our modern world, a
misunderstanding which is based in a simple category error we can expound here:
it is quite real that software is designed for "you," that so many factors come
together to create the "experience" which is undoubtedly for the benefit of the
you. The project of both enterprise and personal computing is one which holds as
an axiom the mythological idea of the "end user," that figure who is at one end
of an architecture, who receives the ultimate phenomenon that is the work of
programming, but to get to this point there are necessary things to install, to
implement, and to then hide from this user. The user is at once the privileged
subject, that figure that the entire machinery operates for, but they are also
the one agent in this whole setup who is precisely ignorant, the only figure who
at the end of the day knows only the shadows. The requirement of the end-user's
ignorance is one of both "safety" and simple practicality, it allows a computer
to provide "services" to anyone, it creates a computer that is for you. The
influential conceptual edifice of the "Model-View-Controller" is a perfect
consolidation of this idea:

(TODO put image here)

Under this influential concept, the programmer thinks in terms of three: the
Model represents, to put it simply, the shape of the data, the actual threshold
where transactions are made on a dynamic store of data; the "view" is what our
user "sees", how otherwise raw data is taken, transformed, and presented qua
spectacle to the customer of the service, this is the land of UI/UX; finally,
the "controller" is separated out here to abstract away the possible
interactions the user can make to the application, the way these interactions
are audited, or as it is put "sanitized," so that a safe and scalable moment of
contact with the model (or rather, the database which the model represents) can
occur.

Conceptual tools like the MVC have, at this point, been reified into frameworks
that are in wide circulation on the internet: Ruby on Rails is a self-contained
framework to create web applications, and follows this arcitecture closely.

With a concept of the MVC, we can approximate a certain story that tells a lot
of how we got to where we are now: a dynamic store, a database, is where we
locate much of the use, the power, of our computers. From a business
perspective, such large stores of information can be quite valuable, can help
triangulate ads for users, and largely provides the sense of space and place, as
Wendy Chun notes, of the way time can map to TODO. For it does not, really,
matter how many calculations a CPU can do in however many nanoseconds, if it is
not in the service of retrieving data, or rendering the view of the data, in
ever more shiny and sophisticated ways.  

Mackenzie Wark argues that the rise of "big data" is also the closure of
"capitalism," understood in the classical Marxist sense. Posing a world where
the insistent feedback of capital to data to capital changes the scene. TODO

> The most obvious aspect of vectoral rule in everyday life is its monopoly of attention, although it is not reducible to this. As Yves Citton notes, in a world awash in digital data, what is rare is the attention paid to it...The rise of technology, financialization, neoliberalism, and biopolitics appear as effects of the same transformation of the forces of production, putting pressure on the relations of production, to the point where what bursts forth is a new ruling class formation.

As one tends to find when the deal with computers, everything is both synecdoche
and token of some lower process. The macro situation of data, surveillance, and
the commodification of a certain kind of universally neurotic "end-user," is
precisely instantiated in a given web application, but also this instantiation
is really nothing but the true site of the process.

While the model, the database, the dynamic store of bits is taken as only one
piece of the whole experience, it must be understood as the privileged,
generative piece. The software is not "for you," even if it is your experience
with it that is given so much care; that aim is only to faciliatate the
interaction with the model, as that is where the money is. One could imagine
twitter without its interface, and in fact, their generous API makes this quite
possible, but it would be hard to imagine twitter without its capacity to
continually accumulate the dynamic data of tweets, mapping them onto time, and
intermingingly them infinitely. The internet itself is nothing but a
geographical infrastructure that faciliatates nothing but the possibility of
relations between the user and the database. _Everything else is just details._

## Reclaiming the database

To me, databases are scary! Not so much in a technical sense, although im sure
some of the finer points on the relation between classic set theory and the
common Relational Model is quite deep and esoteric. Rather, the database is
scary like court date is scary: it represents that edge of an institution where
you can be implicated, where you can 
