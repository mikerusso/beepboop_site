---
title: Site
date: 2018-05-18 
tags: ["fiction"]
---


**Site**
<!-- <div id="sketch"></div> -->

The colors, arranged around a common denominator of heat and asphalt,
swam slowly around. The night had exhausted any possible enthusiasm he
was going to have today. Outside the big yellow box of a building, they
sat, exhausted. It would still be a few hours before the wind picked up,
and at 8:45 in the close quarters of downtown, the exhaust was more than
offensive. He refocused his eyes and she exhaled her sighed smoke.

Night had brought the usual close calls and perambulatoric nonsense. The
nocturnal city was a dungeon---always starless and so with a ceiling and
everything. But they had found a nice little spot for a few hours, and
the old floaty raft was cached in a safe space today.

One time, over stolen whiskey and plastic fume fire, a generic bearded
poetaster envisioned the night floods to him from an invisible, still,
and close moon. The broken grid, abstracted to simple lines from such a
distance, from 40th to MLK, slowly being filled in each night. An
inconceivably methodical, deliberate collection of straightedges and
sharpies engaged in symphonic simultaneity. *The grout cleanse*, he
yelled---it might be nicknamed, he said---if anybody cared enough, he
whispered. His overall delivery generally following this sawtooth-like
pattern: each initial statement ex nihilo and proud, before being
qualified out of existence, slumped back into the shadows of his
deteriorating stream.

There is too much actually happening to say it has one particular
*sound*, the drunk continued, but if it did have a sound it would be
something in between the roar of traffic on the old freeway and\... the
static in my ears right now... It is too big and indeterminate to give
it a particular *shape*, he went on in gasps, but if it did have a shape
it would be trapezoidal, with the smaller parallel always feeling
threatened by the larger.

He chuckled, as if this was really clever. This damn city is not
persons!

There was no disqualifying whisper at the end of that one, and he *was*
onto something there, he thought.

Now, the green salty patina line of the nightly gulf water cut the
building above their heads, and a thousand drops dripped all around, but
you kind of just phased it out. Still a few hours before they had a
place to go, and this would not be a good day for scavenging---they had
enough carried over for a few more days anyway.

Sighs smoked to his right.\
They would not be bothered for a little while here, still early. He let
his eyes go again and the yellow and amaranth and cyan patches floated
in amoebic tension again. Cushions, irradiated, and floating. Morning
site.

\* \* \*

Groans and an itchy kind of movement in the upper registers as the city
awoke. The clouds god mountains in the immanent horizon between
buildings, dwarfing the two as he looked down and she shuffled her
sheets and they walked northish. In the regular morning choreography,
the early birds, and then the rest, started to come down around them:
two at a time, usually, in the squeaky rickets of their external, analog
elevators. The sheer concrete 100 story walls were all at once wet,
sweating drips of load bearing lines and window carriages as the day
became full blown and everyone descended from their higher, more verbed
stories.

Rough hands and worn plastic buckets---silent labors, lift ups and
always the soreness of another damn day. As each carriage landed, the
darkened travaillers jumped over the railing with their tools, deft and
silent, tying off their carriage first and beginning to gather and
account their stuff second. The all at once cascade of landing carriages
with accompanying simple clanks, denim shuffles, ropes unfurling,
(tensing and wearing imperceptibly on brown and black calloused hands),
made a continual dull falling sound all around them, a waterless
waterfall of the new day.

The sight and sound of everything falling into the innumerable spaces
codified by habits and ancient squatter rights---a not so harmonious
shared necessity. They were all silent now, solipsed---faceless drones,
reduced by rigamarole, existent like worn out but functional tools.
Tensions and lines tied off, worn sections of aluminum poles reinforced
here and there by thick tape. These light rickety carriages would all
rise again before the night, with the souls and food and new scars that
completed the daytime phenomena of these people and their
lives---testifying to something he could not quite articulate.

With most having made landfall, awoken by the sun and the
ever-growing-more-mortal clouds, banter emerged slowly: a whistle from
far away, a few whatados elsewhere, *take it its yours, today will be
the day, here I figured you could just hold it like this, she is still
sick, will she ever get better?* and the rumble of a crowd proper
spilled over. As if by a power external to all of them, the entire tone
of the scene switched *on* and the drones became people and the
movements gained meaning, and a semiotic clusterfuck of consciousness
and busyness known all too well to the two somewhat aliens erupted.

There was nothing but trouble now. Only the overture was worth an
audience. He coulda taken them north by culverts, alleys, but he had
wanted to hear that falling sound today. She began shuffling, looking,
writing her cards a little more quickly now.

They were about to turn left, to take the last road west that would exit
downtown and end up near the path to the Center, when he, having been a
little more intense about looking down once the bustle took this final
form, bumped a large tool bag made of stiff fabric and zipties, filled
with greasy pliers and paislied mallet handles, down from the corner of
a carriage's railing. The owner, large, daisy yellow eyes, back turned,
turned immediately and through narrowed slits and well earned inductive
reflex quickly pinged mischief onto him the bumper. Without otherwise
moving at all, the ship yard worker reached out and grabbed him by his
thick flannel, contracting his arm slowly to lift him up a good foot
from the ground.

"Give it back."

"Oh no, not nope, no"

"Give whatever it is fucking back"

"\...sosay. No"

Now even thinner flower eyes rendered disgust at incoherence and now
apparent sour smell. He dropped him simply and began to quickly account
of the tools that were everywhere, with the crowds having to walk over
or just kick them. Fallen back, crumpled and hurt even only from the
foot drop, he looked up after a few moments and saw the man collecting
his, as it turned out, unpilfered collection before moving along without
even another glance.

She was already two blocks ahead, a real mirage of tiny whiteness among
the brown blotches, moving along just as they had been, in either
ignorance or strategic dismissiveness of what had just transpired. He
had to, carefully, weave through the crowds to catch up, only to
manually turn her around because they had passed their street.

\* \* \*

The Center was located in the otherwise desolate northern edge of the
small city. The tide did not reach there, but it was out of the question
that they could spend their nights outside of the metro-proper: too many
bugs, encampments, variables. Shelter and rest in the upper regions of
the city was hard, not guaranteed, but ultimately safe, and since they
were two it was easier to scale the good spots.

Years before, he used to stay out in the high deserts. He had a tent and
was alone. He would watch the upper-atmosphere lightning roll and
penetrate through otherwise invisible clouds in that canvas; ancient
forces terrorizing ascendent worlds thankfully far removed from his own.
Such nights were as a sleeping beast in relation to him: animaled,
inspiring of reverence, expanding and contracting in a rem depth
mysterious and removed.

In those days he had learned to smell for the bugs, and knew when to
stay in the tent. It was really nothing but a large tarp he could place
over a larger trivet of sorts he had found one day, rusting in a field.
In order to remain completely insulated he would sit in the center,
pinning the ends of the tarp down with his butt. The bugs would thunder
from the northeast, if they came, starting around 2 in the morning. And
he would sit in the dark and bite his chapped lip as the sound grew. He
would never know exactly when they would hit, their sound was shepardey
and nauseating: sinister overlapping overtones, increasing forever.
Pitch and volume melding fluidly such that you could never be sure
exactly when the highest point had been reached, the apotheosis
immediately before they hit. But when they did he immediately
disappeared into sound and little infinite bumps. He would wake having
never fallen asleep it seemed, spending moments reremembering things
about his life, breathing slowly.

\* \* \*

The Center was far from the center of any foci he could think of. It was
tucked in among concrete and vine a mile out; practically arboreal, well
shaded most of the day. It had a kind of errant, tropical aura to
it\...at least from the angle they would approach it---after the road
had stopped and the spillway climbed and the more conspicuous camps
avoided. Cloudless, evaporated blue white sky framed pretty, fuzzy
pictures of dilapidated service tunnels and rusted crusted chainlinks.
Bugs (not those) would bzz and hum around otherwise silence,
establishing a circular temporality and expansive space unlike the city.
The building that was the Center seemed to at once slump and stand
proud, and it was really just truly *among* everything*.*

The lady at the desk lived there and she opened it up and locked it down
in mostly regular intervals, with a weekless discrimination of time (and
space and supplies) shared by most of the patrons and really everybody.
The two were regulars and he was established enough here to do his
project despite the space it could take up.

Entering, plastic-filtered light from big windows gave interrupted
golden rectangles to folded-out tables, old small chairs, columns of
magazines. A man, more than half consumed into a large frayed coat it
seemed, sat twiddling twigs and wire into primal jewelry, or scale
modeled tumbleweed. She immediately went and sat in a table near his
corner, shuffling cards, writing here and there, smoking, being gilded
and oblivious. He had started to feel his chest hurt. He looked over at
his corner, bare, and worry creeped. In the back near the lady was his
blue locked plastic tub and he dragged it out, sliding it on broken
tile, giving a not-unpleasant scratching sound against the dirty floor.
The lady at the desk smiled at him although he did not notice.

The work was a kind of sculpture. It had been made in segments---thick
tiles of wood, pigment, geography that were pieced together each day
into the complete edifice. When fully assembled it seemed to smear a
whole corner of the so-called atrium of the Center: two intersecting
walls and the respective floor and ceiling adjacent became, from initial
distance, a purple and red and white gash in the unprepared retina.
Panning the room, perhaps, in unreflective disinterest, the thing would
be liable to all at once drop in you, threaten you---give a slow
sucking-in felt in the gut---an inverted, metaphysical tilt-shift. Its
simple blur, though, its broad swirl, promised more, and approaching it,
you would begin to notice, here and there, stuff of note.

Eg white rolling hills rolling out of the wall to the left, the drastic
nature of their gushing reified in splatters on the wall not formally a
part of the piece. The rumble of a cosmic primary mudslide. As soon as
they are on their way, the generality of these melting-together eggshell
curves are diversified, reabstracted---that is, they are filed down to
threads that blacken and knot and kudzu into human twist tie figures.
Dark shadows of make believe lives in strange intimate detail---like,
there is this one scene where the person has lost their dog, and it was
his last friend, and if you spend enough time, looking down on this
umbra, you will find this dog, looking scared, and too far away to ever
be found. Soon after this the web is invaded, wooshed, taken to task all
at once by a larger-than-(twist-tie)-life glass parade. Violet and
penetrating waves, coming from the other direction, without warning,
wash away everything in their path. Like dust in the wind the black web
world disappears into the shiny expanse---person, dog, and all. Then,
reaching the right wall, the hyaline contours broaden and lose shade.
They become adorned, at their crests, irregularly with found emeralds of
polished bolts, seashelled bottle caps, rusted nickles. And even closer
to the wall, the emeralds then multiply into a pure penumbra of busyness
and sharp trash. A brown static, a train model of ground zero. The trash
extends as a brown, shocking band for the first half foot or so along
the (right) wall meeting the white universe hills at various tangents.
What happens then is a pagination. The thickness of vegetal pages on the
right wall, oblivious of and autonomous from the apocalyptic drama
below, is living out its days in an inward turn, an unmoved flutter. Its
imagined sound nonetheless balancing the spectrum produced by the rumble
of fluid mudslide whiteness to the side, and violet tsunami below: an
upper frequency whitenoise seaside reprieve. These flared pages motivate
the eyes left again, where it started, for some reason. And here, above
the white gushing, taking up the upper meter of the wall, is Bob. Bob's
a sea man, of paper and spit, and there is a frog on his head. But the
frog is also a lantern and it greenifies the whole quadrant. There is a
story to Bob, this is certain to anyone, but his eyes defy expression.
They are everywhere and inward, barely personed. The frog's eyes are
closed, enjoying the fluttery breeze from the other side of the wall.
Together, they are neither implicated nor separated from the rest of the
piece, but are rather metonymical to it, containing the whole totality
in their complicated dualism---frog and Bob. Finally, right at the tip
of the frogs little, peaceful head a single string leads to the ceiling,
and here, a kind of simple, formal transcendence happens: a clap, a
million static energy balls of copper and bright yarn. A generator, the
final chronological piece. Humming over everything. And you realize
their relation to the white gushing, as catalyst, and you begin
again\...

\* \* \*

As he was finishing this assembly for the day other people had come in.
Someone, creaky and wrinkled, with a bright yellow hat, had come in and
began to pluck on a small childs guitar he brought with him. It had only
two strings, intervaled a fourth, about, and the imperfect tinny
repetitive plucks the guy started began to forever ting---*ting ting
ting ting*. The sound never changed, the neck was warped and so the
frets unavailable, but each *ting,* hypnotic, seemed to fall and lay on
the floor, a trickle slowly flooding the golden room with cumulative
fluid units of *tings*. The man in the big coat with little tumbleweeds
all around had some things to say to noone in particular, but his voice
reacted to the *ting ting ting*s sometimes, getting louder or more
rhythmic: *take two of these, i saw once, you take two, and you can
fashion it and bear it, like I did.* Another person, red faced and
dazed, laid against a corner, as if fallen, looking on at dust and
cataracted magazine columns.

And so now *tings* made golden by the light began imperceptibly bouncing
off his sculpture thing, and he looked upon it with simple eyes---just
standing there, hips forward, arms out, a slightly open mouth. There was
this feeling that had loomed before: terrible closure. That is: What
else could be done, really? How many more months of editing could he get
away with? Every small adjustment, every new twist-tie person or rusty
emerald, just kept denying the ultimate fact of the matter: that it was
done. He was done. And he had never felt worse about it.

Some tacit shell of an idea had operated on him for these years of
squatting, half minded, twisting ties and placing trash---looking
reverently into Bob's eyes. He had reveled in its little zones of
comfort. Curled, wombed in its safe and known little reifications. As a
process, each little piece was a friend, a temperament different from
his own, growing ever closer to him. Yet time passed, notions were
eventually filled in, and all at once\... that was that. Everything
suddenly settled into a demystified earthborn gravity. It was today at
this moment that he acknowledged this---he, there, still and breathing.
It was as if he had struggled to climb some grand mountain only to
arrive, finally, disorientingly, oppressively at the bottom.

And so he stared now, breathing slowly, as it became one giant flaw
before him: truly, indeed, a gash. Inert, doing nothing but absorbing
*ting*s like everything else around him now.

He thought about each scrape of day he lived and wheezed, afraid of
people and useless to everything but *this---*this false substance of
puzzled non-pieces. And the flushed gash in the corner now mirrored
nothing and said nothing. It had begun as one itch to scratch and ended
with an itchless and so numb body simply and solely and not even really
that solemnly placed.

And so the gash gashed before him and seemed to creak, tired. He stood
and breathed and blinked, expressionless. They would be here for another
few hours, but then he would be packing it up again, putting the lock on
it again, sliding it back, next to the ladies' desk. They would be
packing up their own supplies, then, onto sore shoulders, hurt legs, not
even looking at each other as they head back down to now-dusky grass,
chains, spillways. They would be reclaiming their raft, hoping for the
same ledge they had found before. Sleeping cold and wet after eating dry
oats, saccharine cans of fruit that hurt their dry mouths, rotten teeth.
Waking before the sun to catch the raft before the water went down
again. Taking the same dawned paths: waiting by yellow and
patinaed-green buildings, blurring eyes, watching cushions. Sighing
smoke. Phasing it out. Walking. Heart jumping at each person. Walking.
Waiting, practically overheating, finding small pieces of oxidized
copper in sharp, scratchy crevices. Coming back, sliding the tub.
Assembling again, editing, then packing it up\... And he saw it all and
the gash just gashed, and rested on itself, and was all at once
calamitous to him.

And the *tings* were about to their knees now, reverberating against
each other and in invisible caverns, slowly giving off deeper octaves of
feedback. The coat, virtually bodiless at this point, was still talking,
saying things like *well I got it over at Septown and it hasnt broke yet
she told me it would but*. Golden mote systems condensed the air,
blanketing everyone, bringing everyone closer by conduction. *ting ting
ting.* And he just stood, motionless.

But so she had been writing, smiling, soft freckled cheeks twitching,
listening to the *ting*ing. And in some kind of subconscious, peripheral
reflex---his lack of movement, his longer breath interval, his energy in
the room---she looked up at him. She looked up and looked into him in a
way she had done only rarely. She saw him and was sad because he was
sad. The sheer sameness of each of their days required little
conversation, but this did not diminish the empathy that rested,
circular as a sleeping cat (as she imagined), in her. Their tacit,
simple companionship---originating, geographically, right where they
were now---was as a body to a soul: simple, unthought, irreducibly
lived.

In this, she never asked him about it, or even thought of something to
ask. Each day sedimented a shared passivity, a jinx-avoidance between
them about their projects. And besides, she knew enough to understand
that it was not a matter of the sculpture thing being nice, being
pretty, being successful at something. She knew, even, that it did not
matter that she liked it---that it reminded her of mountains and fires
and bird's nests and many other things.

Rather, it was simply a matter, now, of time expended. Each day,
stumbling by, was incrementally harder for them. Soon, from what she
understood, the ships would be finished and all those people would leave
this place. But they had no plan, and even if they could find their way
on one, what then? How many years? Of what?

*and i told her, you cant make plastic bend like this, if if if you try
its not going to work, Septowns got alot,*

*ting ting ting*

It was by some momentum of displacing themselves into these things that
they just kept going, and this was the worst thing for him, to be fully
displaced---to stop gerunding, and only be noun. She thought something
like this, but by what argument could she help him? Coimplied in all
this was a simple, understandable, merciful death; one that she saw
resting, with arms crossed, smirking, leaking inside him every night as
they, cold and wet, laid under the cloudy silent static waves above. He
was old and everything was passing by and there was just less and less
to rest against. She looked at him, at Bob, and looked at the flutters
of old books, at the violet waves, and she wanted, for a second, to cry.

*ting ting just jus watch it watch it, you bend it forrrward and\... it
holds up! I leave it by fires and they ting ting ting*

So she shuffled together the cards, rose, and faced him not a foot away.
He just stood toward the gash, still only breathed, and looked on just
barely teary. And the *tings*, well above their heads now, sloshed and
seemed to crawl around, on their foreheads and under their ears, in
through their navels. And the coat said *this* and *that* and *here.*
And holding the cards, and with her twitching pretty, pale face she
began to recite her little things in light, airy reachings:

"Trees seem so nice to me, id watch them all day.

If the world was sideways,

More than now,

We could put planks on the trunks

and build houses or things\..."

And she pulled up the next card and the *ting*ing *ting*ed and the gold
atmosphere of the Center turned to white as the sun peaked and the less
filtered ceiling windows began to bear more of its light.

"I was little, and have grown.

I think about little me like she thought of me

We seem,

I think,

To talk to each other,

even when im silent.

To know each other even though i just knew myself just now"

Here she looked up for a second, with green eyes, to see if there was
any change. He was listening, but that was about it. Her lip curled up,
involuntarily, for a second, her eyes splayed, and the *ting*ing was now
booming through networks and systems around them. And so the next card:

"nuts and cereal and coffee and bean

pillows and floats and fire and spots

colors and plastic and copper and smokes

each hang out,

around me every day"

*ting. Septown.* A glance to him. A new card.

"Thunderboom dont take me

away from sleep and rest

my eyes. and all that

they have felt it all

each night before I sleep."

*ting ting ting* She felt him coming around, barely but yes. And then
tonight everything would be ok\... he could just make something else
now. And they'd have their spot to sleep again and some food and no
booms probably. And they would find a ship soon, and she would get
better too. She would help him talk to people who could help them, and
she would be more aware and keep writing and he would grow older and
make bigger, even greater things. And people would laugh and cry at his
things just like she did so silently next to him. On some big ship out
there.

*ting* and she read another card.

"Spaceplanets fly around, and one takes Margret along,

Through this and that.

now, I bodied myself i tell myself:

Because i wasnt floating around fast enough before,

Because i hadnt heard the dripping yet before this yet,

Because i saw something around the corner and had to check."

*ting ting ting ting ting ting*
